
import SplashScreenComponent from './Components/SplashScreenComponent'

import MoviesComponents from './Components/MoviesComponents';
import SeriesComponents from './SeriesComponents/SeriesComponents';
import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";
import MovieDetails from './Components/MovieDetails';
import SeriesDetails from './SeriesComponents/SeriesDetails';


function App() {
  
  return (
    // <div className="App" >
    //   <SplashScreenComponent/>
    // </div>
    <Router>    
    <Switch>
    <Route exact path= "/" component={SplashScreenComponent}/>
   <Route  exact path="/movies" component={MoviesComponents}/>
   <Route path="/series" component={SeriesComponents}/>
   <Route path="/moviedetails/:id" component={MovieDetails}/>
   <Route path="/seriesdetails/:id" component={SeriesDetails}/>
   </Switch>
   </Router>

  );
}

export default App;
