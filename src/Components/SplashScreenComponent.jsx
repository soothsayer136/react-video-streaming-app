import React, { Component } from 'react';

import '../style/style.css';
// import SeriesComponents from './SeriesComponents';
// import MoviesComponents from './MoviesComponents';
import { Link } from 'react-router-dom';
import SeriesComponents from '../SeriesComponents/SeriesComponents';


class SplashScreenComponent extends Component {

    
   
    render() {
        return (
        
            <div className="SplashRelative" style={{backgroundColor:'black'}}>
            <h3 style={{color:"#BB2D3B",fontFamily: 'Fira Sans', fontWeight:400, marginLeft:20}}>TVSHOWS APP</h3>
                <div className="SplashAbsolute">
                <div  style={{display:"flex", flexDirection:"column", alignItems:"center", marginBottom:30}}>
                    <h1 style={{color:'#5B5445', fontFamily: 'Fira Sans', fontWeight:900, marginBottom:20}}>TVSHOWS APP</h1>
                    <h2 style={{color:'#5B5445', fontFamily: 'Fira Sans', fontWeight:600}}>VIEW LATEST MOVIES AND TV SHOWS</h2>
                    </div>
                    <div>
                 <Link to='/movies'> <button  className="btn btn-danger buttonStyle" style={{marginRight:40, fontFamily: 'Fira Sans'}}>Movies</button> </Link>
                   <Link to='/series'> <button  className="btn btn-danger buttonStyle">TV Shows</button></Link>
                    </div>
                    <div style={{position:'absolute', top:400, left:250, color:"purple", fontWeight:800}}>
                        <p>React TV</p>
                    </div>
                 </div>
            </div>
        );
    }
}

export default SplashScreenComponent;