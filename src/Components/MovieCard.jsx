import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import MovieDetails from './MovieDetails';

class MovieCard extends Component {
    
    render() {
        const baseImgUrl = "https://image.tmdb.org/t/p"
        const size ="w500"
        
        return (
            <div>
             {/* {console.log(this.props.movieCard.title)} */}
            
            <Link to= {`/moviedetails/${this.props.movieCard.id}`} style={{textDecoration:"none"}}> <div className="card" style={{backgroundColor:"cyan",width:250, height:320, alignItems:'center', marginBottom:40, marginLeft:40, border:"none", borderRadius:10}}>
                     <img src={`${baseImgUrl}/${size}${this.props.movieCard.poster_path}`} width="250" height="250"/>
                     <div className="card-body">
                    <h5 className="card-title" style={{color:"#000"}}>{this.props.movieCard.title}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Votes: {this.props.movieCard.vote_average}</h5>
                    {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                    </div>
            </div>
            </Link>
            
            </div>
        );
    }
}

export default MovieCard;