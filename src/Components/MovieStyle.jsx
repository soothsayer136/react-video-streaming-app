import axios from 'axios';
import React, { Component } from 'react';
import MovieCard from './MovieCard';
// import MovieCardSearch from './MovieCardSearch';
import MovieSearch from './MovieSearch';

class MovieStyle extends Component {
   state= {
       movie:""
   }

   search() {
    let {movie} = this.state;
    const url = `https://api.themoviedb.org/3/search/movie?api_key=2da89c7dbab4b2245c428d75e96de340&language=en-US&query=${movie}&page=1&include_adult=false`
    axios.get(url)
    .then((response) =>{
        // console.log(response)
           
           this.setState({
               movie:response.data.results
           }) 
           console.log(this.state.movie) 
             
    }).then(function(error){

    })
 


    // if (movie !== "" && )
}


    render() {
        return (
            <div style={{backgroundColor:"black", height:"auto", width:"100%"}}>
                <h3 style={{color:"#BB2D3B",fontFamily: 'Fira Sans', fontWeight:400, marginLeft:20}}>TVSHOWS APP</h3>
                <div style={{backgroundColor:"grey"}} >
                <p style={{marginLeft:30}}>Home/Movies</p>
                </div>
                <input type='text'
                name="movie_name" 
                placeholder="   Search Movies"
                value={this.state.movie.movie_name}
                onChange={(e) => this.setState({movie:e.target.value})}
                 style={{borderRadius:15, marginLeft:550, width:400, height:40, border:"none"}}/>
                 
                <button className="btn btn-primary" style={{borderRadius:15, height:39,marginTop:-5, alignItems:'center'}} onClick={() => this.search()}>Search</button>
                

              
                <div className="container-fluid" style={{marginTop:50, display:'flex', flexWrap:'wrap'}}>
                {this.props.filmData.map((data, index) =>{
                    return <MovieCard movieCard ={data} key={index}/>
                })}
                </div>

                
                
            </div>
        );
    }
}

export default MovieStyle;