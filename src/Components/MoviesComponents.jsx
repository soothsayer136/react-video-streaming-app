import React, { Component } from 'react';
import axios from 'axios';
import Allmovies from './MovieCard';
import MovieStyle from './MovieStyle';

class MoviesComponents extends Component {

    constructor(){
        super();
        this.state ={
           movieData:[],
           

        }
    }
    componentDidMount(){
      this.getAllMovies();
    }

    

    getAllMovies(){
        let self = this;
        const url ="https://api.themoviedb.org/3/movie/popular?api_key=2da89c7dbab4b2245c428d75e96de340"
        axios.get(url)
        .then(function(response){
            // console.log(response.data)
            self.setState({
                movieData:response.data.results
            })

          
           
        }).then(function(error){
            
        })
        // console.log(self.state.movieData)
    }


   
    
    render() {
        return (
            <div>
            
                <MovieStyle 
                filmData={this.state.movieData}
               
                />
              
            </div>
        );
    }
}

export default MoviesComponents;