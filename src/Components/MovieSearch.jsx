import React, { Component } from 'react';
import axios from 'axios';
import MovieSearchPass from './MovieSearchPass';

class MovieSearch extends Component {
    constructor() {
        super();

        this.state={
            movie:[]
        }
    }
    search() {
        let {movie} = this.state;
        const url = `https://api.themoviedb.org/3/search/movie?api_key=2da89c7dbab4b2245c428d75e96de340&language=en-US&query=${movie}&page=1&include_adult=false`
        axios.get(url)
        .then((response) =>{
            // console.log(response)
               
               this.setState({
                   movie:response.data.results
               }) 
               console.log(this.state.movie[0].title)   
        }).then(function(error){

        })
    }

    render() {
        return (
            <div>
                <input type='text'
                name="movie_name" 
                placeholder="   Search Movies"
                value={this.state.movie.movie_name}
                onChange={(e) => this.setState({movie:e.target.value})}
                 style={{borderRadius:15, marginLeft:550, width:400, height:40, border:"none"}}/>
                 
                <button className="btn btn-primary" style={{borderRadius:15, height:39,marginTop:-5, alignItems:'center'}} onClick={() => this.search()}>Search</button>
                <MovieSearchPass movieInfo={this.state.movie}/>
                
            
            </div>
        );
    }
}

export default MovieSearch;