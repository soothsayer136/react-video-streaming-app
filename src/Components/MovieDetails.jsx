import React, { Component } from 'react';
import axios from 'axios';
// import querySearch from 'query-string';
// import { useParams } from 'react-router-dom';
import MovieVideo from './MovieVideo';



class MovieDetails extends Component {
        constructor(props) {
            super(props);
            this.state ={
                movieDescription:[],
            }
           
            
        }


    componentDidMount(){
        this.getMovieDetails()
        console.log("here")
    }
  
    

    getMovieDetails() {
        let self = this;
        const {id}= this.props.match.params
        console.log(id)
        
        
        const url =`https://api.themoviedb.org/3/movie/${id}?api_key=2da89c7dbab4b2245c428d75e96de340`
        axios.get(url)
        .then(function(response){
            // console.log(response.data)
            self.setState({
                movieDescription:response.data
            })
            // const {poster_path} = self.state.movieDescription
            // console.log('state',self.state.movieDescription.overview)
           
        }).then(function(error){
            
        })
        
    
}
    render() {
        const baseImgUrl = "https://image.tmdb.org/t/p"
        const size ="w500"
        const {poster_path, title, overview, genres, tagline} = this.state.movieDescription;
        
        return (
            <div>
             <div style={{backgroundColor:"black", height:"auto", width:"100%"}}>
                <h3 style={{color:"#BB2D3B",fontFamily: 'Fira Sans', fontWeight:400, marginLeft:20}}>TVSHOWS APP</h3>
                <div style={{backgroundColor:"grey", height:50}} >
                <p style={{marginLeft:30, fontSize:30}}>Home/Movies</p>
                </div>
                <div style={{display:"flex"}}>
                <div style={{marginLeft:50, marginTop:30}}>
            <img src= {`${baseImgUrl}/${size}${poster_path}`} width="400" height="600" />
            </div>
            <div style={{marginLeft:110, marginTop:30}}>
                <h2 style={{color:"#fff",fontFamily:'Roboto'}}>{title}</h2>
                <hr style={{width:"700px", color:"#fff"}}></hr>
                <p style={{color:"grey",fontFamily:'Roboto', fontWeight:800}}>Description</p>
                <p style={{color:"#fff",fontFamily:'Roboto', width:700}}>{overview}</p>
                <hr style={{width:"700px", color:"#fff"}}></hr>
                <p style={{color:"grey",fontFamily:'Roboto',fontWeight:800 }}>Genre</p>
                <hr style={{width:"700px", color:"#fff"}}></hr>
                <p style={{color:"grey",fontFamily:'Roboto',fontWeight:800 }}>Tagline:</p>
                <p style={{color:"#fff",fontFamily:'Roboto'}}>{tagline}</p>
                <h1 style={{color:"#fff",fontFamily:'Roboto', marginLeft:350, marginTop:70, marginBottom:40}}>{title} Trailers</h1>
                <MovieVideo id ={this.props.match.params}/>
                
            </div>
            </div>
            </div>
            </div>
        );
    }
}

export default MovieDetails;