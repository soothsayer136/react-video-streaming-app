import axios from 'axios';
import React, { Component } from 'react';
import SeriesCard from './SeriesCard';
// import MovieCardSearch from './MovieCardSearch';
// import MovieSearch from './MovieSearch';

class SeriesStyle extends Component {
   

  


    render() {
        console.log(this.props.seriesInfo)
        return (
            <div style={{backgroundColor:"black", height:"auto", width:"100%"}}>
                <h3 style={{color:"#BB2D3B",fontFamily: 'Fira Sans', fontWeight:400, marginLeft:20}}>TVSHOWS APP</h3>
                <div style={{backgroundColor:"grey"}} >
                <p style={{marginLeft:30}}>Home/Movies</p>
                </div>
                {/* <MovieSearch/> */}
                

              
                <div className="container-fluid" style={{marginTop:50, display:'flex', flexWrap:'wrap'}}>
                {this.props.seriesInfo.map((data, index) =>{
                    return <SeriesCard seriesCard={data} key={index}/>
                })}
                </div>

                
                
            </div>
        );
    }
}

export default SeriesStyle;