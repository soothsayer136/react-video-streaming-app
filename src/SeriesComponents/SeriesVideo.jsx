import React, { Component } from 'react';
import axios from 'axios';

class SeriesVideo extends Component {
    constructor(props){
        super(props);

        this.state={
            resultyt:[]
        };
    }


    componentDidMount(){
        this.getSeriesTrailer()
    }
    getSeriesTrailer() {
        let self = this;
        const {id}= this.props.id
        
        
        const url =`https://api.themoviedb.org/3/tv/${id}/videos?api_key=2da89c7dbab4b2245c428d75e96de340`
        axios.get(url)
        .then(function(response){
            // console.log(response.data)
            const resultyt = response.data.results.map(obj => "https://www.youtube.com/embed/"+obj.key)
            self.setState({
                resultyt:resultyt
            });
   
            // self.setState({
            //     SeriesVideo:response.data
            // })
            // const {poster_path} = self.state.movieDescription
            // console.log('state',self.state.SeriesVideo)
           
        }).then(function(error){
            
        })
        
    
}

    render() {
        console.log(this.state.resultyt)
        return (
            <div>
            {this.state.resultyt.map((link, i) => {
                    console.log(link);
                    var frame = <iframe width="300" height="300" src={link} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen controls></iframe>
                    return frame;
            })}
            {this.frame}
            </div>
        );
    }
}

export default SeriesVideo;