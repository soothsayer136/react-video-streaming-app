import React, { Component } from 'react';
import axios from 'axios';

import SeriesStyle from './SeriesStyle';

class SeriesComponents extends Component {

    constructor(){
        super();
        this.state ={
           seriesData:[],
           

        }
    }
    componentDidMount(){
      this.getAllSeries();
    }

    

    getAllSeries(){
        let self = this;
        const url ="https://api.themoviedb.org/3/tv/popular?api_key=2da89c7dbab4b2245c428d75e96de340"
        axios.get(url)
        .then(function(response){
            // console.log(response.data)
            self.setState({
                seriesData:response.data.results
            })

          
           
        }).then(function(error){
            
        })
        console.log(self.state.seriesData)
    }


   
    
    render() {
        return (
            <div>
            
                <SeriesStyle 
                seriesInfo={this.state.seriesData}
               
                />
              
            </div>
        );
    }
}

export default SeriesComponents;