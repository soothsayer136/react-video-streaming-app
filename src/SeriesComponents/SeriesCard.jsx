import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import SeriesDetails from './SeriesDetails';

class SeriesCard extends Component {
    
    render() {
        const baseImgUrl = "https://image.tmdb.org/t/p"
        const size ="w500"
        
        return (
            <div>
            {console.log(this.props.seriesCard.title)}
            
            
            <Link to= {`/seriesdetails/${this.props.seriesCard.id}`} style={{textDecoration:"none"}}> <div className="card" style={{backgroundColor:"cyan",width:250, height:320, alignItems:'center', marginBottom:40, marginLeft:40, border:"none", borderRadius:10}}>
                     <img src={`${baseImgUrl}/${size}${this.props.seriesCard.poster_path}`} width="250" height="250"/>
                     <div className="card-body">
                    <h5 className="card-title" style={{ color:"#000"}}>{this.props.seriesCard.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Votes: {this.props.seriesCard.vote_average}</h5>
                    {/* <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                    </div>
            </div>
            </Link>
            
            </div>
        );
    }
}

export default SeriesCard;